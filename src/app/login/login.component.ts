import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public email: string = ""
  public password: string = ""

  failure: boolean = false;


  constructor(private router: Router) {   }

  ngOnInit() {
    this.failure = false
    //if the user is already logged in
    if(localStorage.getItem('user')) {
      this.router.navigate(['/movie'])
    }
  }

  login() {
    console.log(this.email)
    console.log(this.password)

    // make sure that the user and pass match tho, this should
    // be done in backend or firebase. also encrypt the password
    if((this.email == "mbalsamo" && this.password == "password") || (this.email == "lg" && this.password == "1234") || (this.email == "admin" && this.password == "admin")) {
      localStorage.setItem("user", this.email)
      //if user is logged in, send them to /movies
      this.router.navigate(['/movie'])
      document.location.reload(true) //reload page so it fixes

    }
    else
    {
      this.failure = true;
    }


  }
}
