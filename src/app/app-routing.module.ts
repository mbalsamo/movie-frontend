import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MovieComponent } from './movie/movie.component'
import { AboutComponent } from './about/about.component'
import { HelpComponent } from './help/help.component'

import { LoginComponent } from './login/login.component'
import { AddmovieComponent } from './addmovie/addmovie.component'

const routes: Routes = [
  { //makes it so when you go to /login, it will go to the LoginComponent module
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'help',
    component: HelpComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'movie',
    component: MovieComponent
  },
  {
    path: 'addmovie',
    component: AddmovieComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
