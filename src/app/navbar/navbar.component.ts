import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  loggedIn: boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
    //if they aren't logged in, redirect them to login page
    if(!localStorage.getItem('user')){
      this.router.navigate(['/login'])
    }
    else{
      this.loggedIn = true
    }
  }

  logout() {

    //clears our login keys, so we arent logged in
    localStorage.clear()
    document.location.reload(true) //reload page so it fixes
    this.router.navigate(['/login'])
  }

  // goToADDMOVIE() {
  //   this.router.navigate(['/addmovie'])
  //
  // }



}
