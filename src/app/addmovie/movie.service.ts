import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment'

import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root' //any component can use this
})

export class MoviesService {
  private BACKEND_URL = "http://localhost:5050/movies" //environment.BACKEND_URL + "/movies" //changes to IP of backend when I deploy it, but uses LOCALHOST when nonproduction

constructor(private http: HttpClient) { }

// retrieves all the movies from backend through HTTP GET
  getAllMovies(): Observable<any[]> {
    return this.http.get<any[]>(this.BACKEND_URL)
  }

// adding a new movies
  addNewMovie(movie: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.post(this.BACKEND_URL, movie, options) //we're sending movie
  }

// // removing the movie
  // removeThisMovie(movie : any, id: string): Observable<any> {
  //   return this.http.put(this.BACKEND_URL + "/" + id, data, options)
  // }

// update the movie, given the data and id
  // updateMovie(data: any, id: string): Observable<any> {
  //   const options = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json'
  //     })
  //   }
  //   // http://localhost:5050/movies/1010101010 (thats the movie ID)
  //   return this.http.put(this.BACKEND_URL + "/" + id, data, options)
  // }
}
