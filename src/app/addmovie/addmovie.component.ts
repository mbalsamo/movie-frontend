import { Component, OnInit } from '@angular/core';
import { MoviesService } from './movie.service'
import { Router } from '@angular/router'



@Component({
  selector: 'app-addmovie',
  templateUrl: './addmovie.component.html',
  styleUrls: ['./addmovie.component.css']
})
export class AddmovieComponent implements OnInit {
  public title: string = ""
  public description: string = ""
  public stars: string = ""
  public ageRating: string = ""
  public genre: string = ""
  public timeRunning: string = ""

  movies: any[];


  constructor(private moviesService: MoviesService,private router: Router) { }

  ngOnInit() {
    this.getAllMovies()
  }

  getAllMovies() {
    this.moviesService.getAllMovies().subscribe(
      data => {
        this.movies = data;
        console.log(data)
      },
      error => {
        console.log(error);
      }
    );
  }

  addThisMOVIE() {
    console.log("Title: " + this.title + " Description: " + this.description + " Stars: " + this.stars + " Age Rating: " + this.ageRating)
    //TODO: add the title, description, stars, ageRating,
    //      genre, timeRunning into the mongo database

      const movie = {
        title: this.title,
        description: this.description,
        stars: this.stars,
        ageRating: this.ageRating,
        genre: this.genre,
        timeRunning: this.timeRunning
      }

      this.moviesService.addNewMovie(movie).subscribe( //subscribe means if something happens we'll be notified
        data => {
          console.log("movie submitted " + data)
          this.router.navigate(['/movie'])
        },
        error => {
          console.log("rip, there is error adding: " + error)
        }
      )
  }

}
