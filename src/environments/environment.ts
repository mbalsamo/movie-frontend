// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: "AIzaSyBFoWX7PF-XY9kxC86hvuMPvSZOSTujpgU",
    authDomain: "groovymovie-4506c.firebaseapp.com",
    databaseURL: "https://groovymovie-4506c.firebaseio.com",
    projectId: "groovymovie-4506c",
    storageBucket: "groovymovie-4506c.appspot.com",
    messagingSenderId: "327658684628",
    appId: "1:327658684628:web:1ba40698c2b0f0928ffb41",
    measurementId: "G-PZLEY6R507"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
